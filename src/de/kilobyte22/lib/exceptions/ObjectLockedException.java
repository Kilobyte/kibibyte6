package de.kilobyte22.lib.exceptions;

/**
 * Created with IntelliJ IDEA.
 * User: Stephan
 * Date: 07.03.13
 * Time: 19:33
 *
 * @author Stephan
 * @copyright Copyright 2013 Stephan
 */
public class ObjectLockedException extends RuntimeException {
    public ObjectLockedException() {super();}
    public ObjectLockedException(String message) {super(message);}
}
