package de.kilobyte22.lib.logging;

import java.io.OutputStream;
import java.io.PrintStream;

/**
 * Created with IntelliJ IDEA.
 * User: Stephan
 * Date: 03.03.13
 * Time: 13:29
 *
 * @author Stephan
 * @copyright Copyright 2013 Stephan
 */
public class LogStream extends PrintStream {

    private PrintStream orig;
    private Logger log;
    private LogLevel level;

    public LogStream(PrintStream orig, Logger log, LogLevel level) {
        super(orig);
        this.orig = orig;
        this.log = log;
        this.level = level;
        log.setStream(this);
    }

    @Override
    public void println(String string) {
        log.log(level, string);
    }
}
