package de.kilobyte22.lib.logging;

import com.google.common.eventbus.EventBus;

import java.io.PrintStream;
import java.util.LinkedList;

import static de.kilobyte22.lib.logging.LogLevel.INFO;
import static de.kilobyte22.lib.logging.LogLevel.SEVERE;

/**
 * Created with IntelliJ IDEA.
 * User: Stephan
 * Date: 16.01.13
 * Time: 20:10
 * To change this template use File | Settings | File Templates.
 */
public class Logger {
    static {
        //Class.forName("de.kilobyte22.lib.logging.LogLevel"); // Make sure the LogLevels initialize before replacing any streams
        stdout = System.out;
        stderr = System.err;
        System.setOut(new LogStream(System.out, new Logger("STDOUT"), INFO));
        System.setErr(new LogStream(System.out, new Logger("STDERR"), SEVERE));
    }
    static EventBus eventBus;
    static PrintStream stdout, stderr;
    Logger parent;
    String name;
    private LogStream stream;
    private LinkedList<LogLevel> ignoredLoglevels = new LinkedList<LogLevel>();
    private static LinkedList<LogLevel> staticIgnoredLoglevels = new LinkedList<LogLevel>();

    public Logger() {
        this(null);
    }

    public static void setEventBus(EventBus eb) {
        eventBus = eb;
    }

    public Logger(String name) {
        this(name, null);
    }

    public void setStream(LogStream st) {
        stream = st;
    }

    public Logger(String name, Logger parent) {
        this.name = name;
        this.parent = parent;
    }

    public void log(LogLevel level, String message) {
        if (isLevelIgnored(level))
            return;
        if (parent != null)
            parent.log(level, message);
        else {
            String outline = (name != null ? "[" + name + "] " : "") + message;
            if (eventBus != null) {
                LogEvent e = new LogEvent();
                e.level = level;
                e.logger = this;
                e.message = message;
                e.printmessage = outline;
                eventBus.post(e);
                outline = e.printmessage;
            }
            level.writeLine(outline);
        }
    }

    private boolean isLevelIgnored(LogLevel level) {
        if (ignoredLoglevels.contains(level)) return true;
        if (staticIgnoredLoglevels.contains(level)) return true;
        return false;
    }

    public void log(String message) {
        log(INFO, message);
    }

    public void log(Throwable ex) {
        log(LogLevel.SEVERE, ex.getClass().getName() + ": " + ex.getMessage());
        for (StackTraceElement el : ex.getStackTrace()) {
            log(LogLevel.SEVERE, "    at " + el.toString());
        }
        if (ex.getCause() != null) {
            log(LogLevel.SEVERE, "Caused by " + ex.getClass().getName() + ": " + ex.getMessage());
            for (StackTraceElement el : ex.getCause().getStackTrace()) {
                log(LogLevel.SEVERE, "    at " + el.toString());
            }
        }
    }

    public void addIgnoredLevel(LogLevel level) {
        ignoredLoglevels.add(level);
    }

    public void removeIgnoredLevel(LogLevel level) {
        ignoredLoglevels.remove(level);
    }

    public static void addSIgnoredLevel(LogLevel level) {
        staticIgnoredLoglevels.add(level);
    }

    public static void removeSIgnoredLevel(LogLevel level) {
        staticIgnoredLoglevels.remove(level);
    }

    public LogStream getStream() {
        return stream;
    }
}
