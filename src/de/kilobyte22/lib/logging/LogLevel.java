package de.kilobyte22.lib.logging;

import java.io.PrintStream;

/**
 * Created with IntelliJ IDEA.
 * User: Stephan
 * Date: 16.01.13
 * Time: 20:10
 * To change this template use File | Settings | File Templates.
 */
public class LogLevel {
    public static final LogLevel INFO = new LogLevel("INFO", System.out);
    public static final LogLevel DEBUG = new LogLevel("DEBUG", System.out);
    public static final LogLevel WARNING = new LogLevel("WARNING", System.out);
    public static final LogLevel SEVERE = new LogLevel("SEVERE", System.err);

    private PrintStream stream;
    private String name;
    protected LogLevel(String name, PrintStream stream) {
        this.stream = stream;
        this.name = name;
    }

    public void writeLine(String text) {
        stream.println("[" + name + "] " + text);
    }
}
