package de.kilobyte22.lib.configuration;

public class ConfigNull extends ConfigNode {
    @Override
    public Object toObject() {
        return null;
    }
}
