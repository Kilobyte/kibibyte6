package de.kilobyte22.lib.configuration;

public class ConfigBoolean extends ConfigNode {


    private final boolean value;

    public ConfigBoolean(Boolean value) {
        super();
        this.value = value;
    }

    @Override
    public Boolean toObject() {
        return value;
    }
}
