package de.kilobyte22.lib.configuration;

public class ConfigInt extends ConfigNode {

    private int value;

    public ConfigInt(int value) {
        this.value = value;
    }

    @Override
    public Integer toObject() {
        return value;
    }
}
