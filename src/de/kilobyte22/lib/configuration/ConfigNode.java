package de.kilobyte22.lib.configuration;

import de.kilobyte22.lib.logging.LogLevel;
import de.kilobyte22.lib.logging.Logger;

import java.util.List;
import java.util.Map;

public abstract class ConfigNode {

    protected static Logger logger = new Logger("CONFIGURATION");

    public abstract Object toObject();

    public static ConfigNode toNode(Object o) {
        if (o instanceof Map) {
            return new ConfigMap((Map) o);
        } else if (o instanceof List) {
            return new ConfigList((List) o);
        } else if (o instanceof String) {
            return new ConfigString((String) o);
        } else if (o instanceof Integer) {
            return new ConfigInt((Integer) o);
        } else if (o instanceof Long) {
            return new ConfigLong((Long) o);
        } else if (o instanceof Boolean) {
            return new ConfigBoolean((Boolean) o);
        }
        logger.log(LogLevel.DEBUG, "Invalid type for config option: " + o.getClass().getName());
        return new ConfigNull();
    }
}
