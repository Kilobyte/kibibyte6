package de.kilobyte22.app.kibibyte.exceptions;

/**
 * Created with IntelliJ IDEA.
 * User: Stephan
 * Date: 17.02.13
 * Time: 16:59
 *
 * @author Stephan
 * @copyright Copyright 2013 Stephan
 */
public class CommandException extends RuntimeException {
    public CommandException(String message) {
        super(message);
    }
}
