package de.kilobyte22.app.kibibyte.core;


import de.kilobyte22.app.kibibyte.api.permission.IPermissionHandler;
import de.kilobyte22.app.kibibyte.api.permission.PermissionRegistry;
import de.kilobyte22.app.kibibyte.command.CoreComands;
import de.kilobyte22.app.kibibyte.command.CommandManager;
import de.kilobyte22.app.kibibyte.exceptions.InvalidPluginException;
import de.kilobyte22.app.kibibyte.exceptions.RehashException;
import de.kilobyte22.app.kibibyte.misc.ChannelsSpecialPermissionHandler;
import de.kilobyte22.app.kibibyte.misc.Nickserv;
import de.kilobyte22.app.kibibyte.misc.PermissionSystem;
import de.kilobyte22.app.kibibyte.misc.YamlPermissionHandler;
import de.kilobyte22.app.kibibyte.plugin.PluginSystem;
import de.kilobyte22.lib.configuration.ConfigFile;
import de.kilobyte22.lib.configuration.ConfigList;
import de.kilobyte22.lib.configuration.ConfigMap;
import de.kilobyte22.lib.configuration.ConfigNode;
import de.kilobyte22.lib.event.EventBus;
import de.kilobyte22.lib.logging.LogLevel;
import de.kilobyte22.lib.logging.Logger;
import org.pircbotx.OutputThread;
import org.pircbotx.PircBotX;
import org.pircbotx.exception.IrcException;
import org.pircbotx.hooks.Event;
import org.pircbotx.hooks.Listener;
import org.pircbotx.hooks.events.DisconnectEvent;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.LinkedList;

public class Kibibyte extends PircBotX {
    public static String[] args;
    public static ConfigFile config;
    private static final String KIBIVERSION = "6.0.12";
    public EventBus eventBus;
    public LinkedList<String> prefixes, channels = new LinkedList<String>(), initPlugins = new LinkedList<String>();
    public PermissionSystem permissionSystem;
    public Nickserv nickservSystem;
    public CommandManager commandManager;
    public PluginSystem pluginSystem;

    private String server, spass, opass, ouser, nspass, nick, ident, realname, owner;
    private boolean die;
    private int port, messagedelay, throttleCount, throttleTime;
    private final Logger logger = new Logger("MAIN");

    public Kibibyte(String[] args) {
        logger.log("Initializing Kibibyte.");
        Kibibyte.args = args;
        if (!new File("config").exists())
            new File("config").mkdir();
        prefixes = new LinkedList<String>();
        config = new ConfigFile(new File("config.yml"));
        eventBus = new EventBus();
        commandManager = new CommandManager(this);
        permissionSystem = new PermissionSystem(this);
        nickservSystem = new Nickserv(this);
        pluginSystem = new PluginSystem(this);
        YamlPermissionHandler yph = new YamlPermissionHandler();
        PermissionRegistry.registerPermissionHandler("yaml", yph);
        eventBus.register(yph);
        commandManager.registerHandler(CoreComands.class);
        try {
            rehash();
        } catch (RehashException e) {
            logger.log(LogLevel.SEVERE, "Initial rehash failed: " + e.getMessage());
            System.exit(-1);
        }
        if (die) {
            logger.log(LogLevel.SEVERE, "You haven't edited your Configuration file yet. Please do it and set the misc/die option to false");
            System.exit(-1);
        }

        logger.log("Initialization done.");
    }

    public String getOwner() {
        return owner;
    }

    public void rehash() throws RehashException {
        logger.log("Rehashing Configuration");
        config.load();
        ConfigList plist = config.getList("prefixes");
        prefixes.clear();
        for (ConfigNode node : plist) {prefixes.add(node.toString());}
        ConfigMap connectionSettings = config.getMap("connection");
        server = connectionSettings.getString("server", "irc.example.net");
        spass = connectionSettings.getString("serverpass", "");
        opass = connectionSettings.getString("operpass", "");
        ouser = connectionSettings.getString("operuser", "");
        nspass = connectionSettings.getString("nickservpass", "");
        port = connectionSettings.getInt("port", 6667);
        nick = connectionSettings.getString("nick", "Kibibyte");
        ident = connectionSettings.getString("ident", "kibibyte");
        realname = connectionSettings.getString("realname", "");
        owner = connectionSettings.getString("owner", "PutYourNickHere");

        ConfigMap miscSettings = config.getMap("misc");
        die = miscSettings.getBoolean("die", true);
        String permHandler = miscSettings.getString("permissionhandler", "yaml");
        channels.clear();
        for (ConfigNode node : miscSettings.getList("channels")) {channels.add(node.toString());}
        initPlugins.clear();
        for (ConfigNode node : miscSettings.getList("autoloadPlugins")) {initPlugins.add(node.toString());}
        messagedelay = miscSettings.getInt("messagedelay", 1000);
        setVerbose(miscSettings.getBoolean("verbose", false));
        setAutoReconnect(miscSettings.getBoolean("autoReconnect", true));
        throttleCount = miscSettings.getInt("throttleCount", 5);
        throttleTime = miscSettings.getInt("throttleTimeMillisec", 1000);
        Boolean dbg = miscSettings.getBoolean("debug");
        if (dbg != null && dbg) {
            Logger.removeSIgnoredLevel(LogLevel.DEBUG);
        } else {
            Logger.addSIgnoredLevel(LogLevel.DEBUG);
        }

        IPermissionHandler h = PermissionRegistry.getHandler(permHandler);
        if (h == null) {
            throw new RehashException("Could not find permission handler '" + permHandler + "'. Maybe a plugin isn't loaded");
        }
        permissionSystem.setHandler(h);
        PermissionRegistry.registerSpecialPermissionHandler(new ChannelsSpecialPermissionHandler());
        config.save();
        logger.log("Rehash done.");
    }

    public void restart(String reason) {
        quitServer(reason);
        try {
            final String separator = System.getProperty("file.separator");
            final String javaBin = System.getProperty("java.home") + separator + "bin" + separator + "java";
            final File currentJar = new File(Kibibyte.class.getProtectionDomain().getCodeSource().getLocation().toURI());

            if (!currentJar.getName().endsWith(".jar"))
                return;

            final ArrayList<String> command = new ArrayList<String>();
            command.add(javaBin);
            command.add("-jar");
            command.add(currentJar.getPath());

            final ProcessBuilder builder = new ProcessBuilder(command);
            builder.start();
            System.exit(0);
        } catch (Exception ex) {
            logger.log(ex);
        }
    }

    public void restart() {
        restart("Kibibyte v" + VERSION + " restarting...");
    }

    public void run() {
        logger.log("Starting Kibibyte...");
        for(String plugin : initPlugins) {
            try {
                pluginSystem.loadPluginByName(plugin);
                pluginSystem.enable(plugin);
            } catch (InvalidPluginException e) {
                e.printStackTrace();
            } catch (InstantiationException e) {
                e.printStackTrace();
            }
        }
        getListenerManager().addListener(new Listener() {
            @Override
            public void onEvent(Event event) throws Exception {
                eventBus.post(event);
            }
        });
        try {
            setName(nick);
            setLogin(ident);
            if (realname.equals(""))
                setVersion("Kibibyte v" + KIBIVERSION + ", Instance owned by " + owner);
            else
                setVersion(realname);

            if (spass.equals(""))
                connect(server, port);
            else
                connect(server, port, spass);

            if (!(ouser.equals("") || opass.equals(""))) {
                logger.log("Opering up...");
                sendRawLine("OPER " + ouser + " " + opass);
            }
            if (!nspass.equals("")) {
                logger.log("Indentifying...");
                sendMessage("NickServ", "IDENTIFY " + nspass);
            }

            logger.log("Joining...");
            for (String c : channels) {
                joinChannel(c);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (IrcException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected OutputThread createOutputThread(BufferedWriter writer) {
        OutputThread output = new CustomOutputThread(this, writer);
        output.setName("bot" + botCount + "-output");
        return output;
    }

    public int getThrottleCount() {
        return throttleCount;
    }

    public int getThrottleTime() {
        return throttleTime;
    }
}
