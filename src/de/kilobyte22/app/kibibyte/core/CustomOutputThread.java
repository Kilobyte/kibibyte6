package de.kilobyte22.app.kibibyte.core;

import org.pircbotx.OutputThread;

import java.io.BufferedWriter;
import java.util.LinkedList;
import java.util.Queue;

public class CustomOutputThread extends OutputThread {

    Queue<Event> eventQueue = new LinkedList<Event>();
    long lastMessageTime = 0;
    int messagecount = 0;
    Kibibyte bot;
    boolean idle = true;

    public CustomOutputThread(Kibibyte kibibyte, BufferedWriter writer) {
        super(kibibyte, writer);
        bot = kibibyte;
    }

    public void send(String message) {
        failIfNotConnected();
        try {
            queue.put(message);
            synchronized (this) {
                eventQueue.offer(Event.MESSAGE);
            }
            if (idle)
                interrupt();
        } catch (InterruptedException ex) {
            throw new RuntimeException("Can't add message to queue", ex);
        }
    }

    @Override
    public void run() {
        while (true) {
            try {
                String line = queue.take();
                failIfNotConnected();
                if (line != null && bot.isConnected())
                    sendRawLineNow(line);

                if (lastMessageTime  < System.currentTimeMillis() - bot.getThrottleTime())
                    messagecount = 0;

                messagecount++;
                lastMessageTime = System.currentTimeMillis();
                //Small delay to prevent spamming of the channel
                if (queue.isEmpty()) {
                    idle = true;
                    Thread.sleep(bot.getMessageDelay() * 10);
                } else if (messagecount > bot.getThrottleCount()) {
                    idle = false;
                    Thread.sleep(bot.getMessageDelay());
                } else {
                    idle = false;
                }
            } catch (InterruptedException ex) {
                Event e = eventQueue.peek();
                switch(e) {
                    case MESSAGE:
                        idle = false;
                        break;
                    default:
                        return;
                }
            }
        }
    }

    private enum Event {
        MESSAGE
    }
}
