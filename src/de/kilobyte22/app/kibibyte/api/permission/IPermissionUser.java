package de.kilobyte22.app.kibibyte.api.permission;

import org.pircbotx.Channel;

public interface IPermissionUser {
    public boolean hasPermission(String permission, Channel channel);
    public boolean hasParsedPermission(String permission, Channel channel);
}
