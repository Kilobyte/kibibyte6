package de.kilobyte22.app.kibibyte.api.permission;

import de.kilobyte22.app.kibibyte.misc.AccessLevel;
import org.pircbotx.Channel;
import org.pircbotx.User;

import java.util.LinkedList;

public interface IPermissionHandler {
    public Boolean getPermission(String perm, String user);
    public LinkedList<IGroup> getGroups(String  user);
    public void setUserPermission(String user, String permission, Boolean value);
    public IGroup getGroup(String name);
    boolean mayUse(User user, Channel channel);
    void setUseLevel(Channel channel, AccessLevel accessLevel);
    void createGroup(String name);
    void deleteGroup(String name);
}
