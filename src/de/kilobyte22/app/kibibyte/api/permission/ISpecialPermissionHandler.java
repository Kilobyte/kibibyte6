package de.kilobyte22.app.kibibyte.api.permission;

import org.pircbotx.Channel;
import org.pircbotx.User;

import java.util.List;

public interface ISpecialPermissionHandler {
    public List<String> getSpecialGroups(User user, Channel channel);
}
