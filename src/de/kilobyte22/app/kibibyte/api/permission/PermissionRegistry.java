package de.kilobyte22.app.kibibyte.api.permission;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public final class PermissionRegistry {
    private static HashMap<String, IPermissionHandler> permissionHandlers = new HashMap<String, IPermissionHandler>();
    private static LinkedList<ISpecialPermissionHandler> specialPermissionHandlers = new LinkedList<ISpecialPermissionHandler>();

    private PermissionRegistry(){}

    public static void registerPermissionHandler(String name, IPermissionHandler handler) {
        permissionHandlers.put(name, handler);
    }
    public static IPermissionHandler getHandler(String name) {
        return permissionHandlers.get(name);
    }
    public static Map<String, IPermissionHandler> getPermissionHandlers() {
        return permissionHandlers;
    }

    public static void registerSpecialPermissionHandler(ISpecialPermissionHandler handler) {
        specialPermissionHandlers.add(handler);
    }
    public static List<ISpecialPermissionHandler> getSpecialPermissionHandlers() {
        return specialPermissionHandlers;
    }
}
