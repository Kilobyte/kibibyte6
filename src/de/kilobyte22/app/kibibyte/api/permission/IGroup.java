package de.kilobyte22.app.kibibyte.api.permission;

public interface IGroup {
    public Boolean hasPermission(String permission);
    public String getName();
    public IGroup getParent();
    public void setParent(IGroup group);
    public void setPermission(String permission, Boolean value);
    public void addUser(String user);
    public void removeUser(String user);
}
