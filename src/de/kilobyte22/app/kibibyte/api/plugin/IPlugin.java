package de.kilobyte22.app.kibibyte.api.plugin;

import de.kilobyte22.app.kibibyte.plugin.BotAccess;

public interface IPlugin {
    public void load(BotAccess access);
    public void unload();
    public void enable();
    public void disable();
}
