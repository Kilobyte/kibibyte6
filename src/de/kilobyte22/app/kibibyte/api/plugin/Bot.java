package de.kilobyte22.app.kibibyte.api.plugin;

import de.kilobyte22.app.kibibyte.core.Kibibyte;
import de.kilobyte22.app.kibibyte.exceptions.RehashException;
import de.kilobyte22.app.kibibyte.plugin.BotAccess;
import org.pircbotx.Channel;
import org.pircbotx.User;
import org.pircbotx.exception.IrcException;
import org.pircbotx.exception.NickAlreadyInUseException;

import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: Stephan
 * Date: 10.03.13
 * Time: 19:35
 * To change this template use File | Settings | File Templates.
 */
public abstract class Bot {

    protected BotAccess botAccess;
    protected Kibibyte bot;
    public Bot(BotAccess access, Kibibyte bot) {
        botAccess = access;
        this.bot = bot;
    }

    public abstract void join(String channel, String password);
    public void join(String channel) {join(channel, null);}
    public void join(String[] channels, String[] passwords) {
        for (int i = 0; i < channels.length; i++) {
            if (i < passwords.length) {
                join(channels[i], passwords[i]);
            } else {
                join(channels[i]);
            }
        }
    }
    public void join(String[] channels) {join(channels, new String[]{});}

    public abstract void part(String channel, String message);
    public void part(String channel) {part(channel, "Kibibyte v" + Kibibyte.VERSION);}
    public void part(String[] channels) {
        for(String c : channels)
            part(c);
    }

    public void hop(String channel) {part(channel); join(channel);}

    public abstract void quit(String message);
    public void quit() {quit("Kibibyte v" + Kibibyte.VERSION);}

    public abstract void sendRawLine(String line);

    public abstract void sendRawLineNow(String line);

    public abstract void sendMessage(String target, String message);
    public void sendMessage(User user, String message) {sendMessage(user.getNick(), message);}
    public void sendMessage(Channel channel, String message) {sendMessage(channel.getName(), message);}

    public abstract void sendNotice(String target, String message);
    public void sendNotice(User user, String message) {sendNotice(user.getNick(), message);}
    public void sendNotice(Channel channel, String message) {sendNotice(channel.getName(), message);}

    public abstract void sendCTCPCommand(String target, String command);

    public abstract void sendCTCPResponse(String target, String command);

    public abstract void changeNick(String newNick);

    public abstract void reconnect() throws IrcException, IOException;

    public abstract void rehash() throws RehashException;

    /*public abstract void setMode(String target, String mode, String... args);
    
    public abstract void setChannelLimit(org.pircbotx.Channel chan, int limit);

    public abstract void removeChannelLimit(org.pircbotx.Channel chan);

    public abstract void setChannelKey(org.pircbotx.Channel chan, java.lang.String key);

    public abstract void removeChannelKey(org.pircbotx.Channel chan, java.lang.String key);

    public abstract void setInviteOnly(org.pircbotx.Channel chan);

    public abstract void removeInviteOnly(org.pircbotx.Channel chan);

    public abstract void setModerated(org.pircbotx.Channel chan);

    public abstract void removeModerated(org.pircbotx.Channel chan);

    public abstract void setNoExternalMessages(org.pircbotx.Channel chan);

    public abstract void removeNoExternalMessages(org.pircbotx.Channel chan);

    public abstract void setSecret(org.pircbotx.Channel chan);

    public abstract void removeSecret(org.pircbotx.Channel chan);

    public abstract void setTopicProtection(org.pircbotx.Channel chan);

    public abstract void removeTopicProtection(org.pircbotx.Channel chan);

    public abstract void sendInvite(java.lang.String nick, java.lang.String channel);

    public abstract void sendInvite(org.pircbotx.User target, java.lang.String channel);

    public abstract void sendInvite(org.pircbotx.User target, org.pircbotx.Channel channel);

    public abstract void sendInvite(org.pircbotx.Channel target, org.pircbotx.Channel channel);

    public abstract void ban(org.pircbotx.Channel channel, java.lang.String hostmask);

    public abstract void unBan(org.pircbotx.Channel channel, java.lang.String hostmask);

    public abstract void op(org.pircbotx.Channel chan, org.pircbotx.User user);

    public abstract void deOp(org.pircbotx.Channel chan, org.pircbotx.User user);

    public abstract void voice(org.pircbotx.Channel chan, org.pircbotx.User user);

    public abstract void deVoice(org.pircbotx.Channel chan, org.pircbotx.User user);

    public abstract void halfOp(org.pircbotx.Channel chan, org.pircbotx.User user);

    public abstract void deHalfOp(org.pircbotx.Channel chan, org.pircbotx.User user);

    public abstract void owner(org.pircbotx.Channel chan, org.pircbotx.User user);

    public abstract void deOwner(org.pircbotx.Channel chan, org.pircbotx.User user);

    public abstract void superOp(org.pircbotx.Channel chan, org.pircbotx.User user);

    public abstract void deSuperOp(org.pircbotx.Channel chan, org.pircbotx.User user);

    public abstract void setTopic(org.pircbotx.Channel chan, java.lang.String topic);

    public abstract void kick(org.pircbotx.Channel chan, org.pircbotx.User user);

    public abstract void kick(org.pircbotx.Channel chan, org.pircbotx.User user, java.lang.String reason);

    //public abstract void listChannels();

    //public abstract void listChannels(java.lang.String parameters);

    public abstract org.pircbotx.DccFileTransfer dccSendFile(java.io.File file, org.pircbotx.User reciever, int timeout);

    public abstract org.pircbotx.DccChat dccSendChatRequest(org.pircbotx.User sender, int timeout) throws java.io.IOException, java.net.SocketTimeoutException;

    public abstract void log(java.lang.String line);

    public void logException(java.lang.Throwable t) {
        botAccess.logger.log(t);
    }

    public abstract void setVerbose(boolean verbose);

    public abstract void setFinger(java.lang.String finger);

    public abstract java.lang.String getName();

    public abstract java.lang.String getNick();

    public abstract java.lang.String getLogin();

    public abstract java.lang.String getVersion();

    public abstract java.lang.String getFinger();

    public abstract boolean isConnected();

    public abstract void setMessageDelay(long delay);

    public abstract long getMessageDelay();

    public abstract int getMaxLineLength();

    public abstract int getOutgoingQueueSize();

    public abstract java.lang.String getServer();

    public abstract int getPort();

    public abstract java.lang.String getPassword();

    public abstract void setEncoding(java.lang.String charset) throws java.io.UnsupportedEncodingException;

    public abstract void setEncoding(java.nio.charset.Charset charset);

    public abstract java.nio.charset.Charset getEncoding();

    public abstract java.net.InetAddress getInetAddress();

    public abstract void setDccInetAddress(java.net.InetAddress dccInetAddress);

    public abstract java.net.InetAddress getDccInetAddress();

    public abstract java.util.List<java.lang.Integer> getDccPorts();

    public abstract java.lang.String toString();

    public abstract int getSocketTimeout();

    public abstract void setSocketTimeout(int socketTimeout);

    public abstract java.util.Set<org.pircbotx.Channel> getChannels();

    public abstract java.util.Set<org.pircbotx.Channel> getChannels(org.pircbotx.User user);

    public abstract org.pircbotx.Channel getChannel(java.lang.String name);

    public abstract java.util.Set<java.lang.String> getChannelsNames();

    public abstract boolean channelExists(java.lang.String channel);

    public abstract java.util.Set<org.pircbotx.User> getUsers(org.pircbotx.Channel chan);

    public abstract org.pircbotx.User getUser(java.lang.String nick);

    public abstract org.pircbotx.User getUserBot();

    public abstract boolean userExists(java.lang.String nick);

    public abstract org.pircbotx.ServerInfo getServerInfo();

    public abstract org.pircbotx.hooks.managers.ListenerManager<? extends org.pircbotx.PircBotX> getListenerManager();

    public abstract void setListenerManager(org.pircbotx.hooks.managers.ListenerManager<? extends org.pircbotx.PircBotX> listenerManager);

    public abstract javax.net.SocketFactory getSocketFactory();

    public abstract boolean isVerbose();

    public abstract boolean isAutoNickChange();

    public abstract boolean hasShutdownHook();

    public abstract void useShutdownHook(boolean use);

    public abstract void shutdown();

    public abstract <E extends org.pircbotx.hooks.Event> E waitFor(java.lang.Class<? extends E> eventClass) throws java.lang.InterruptedException;

    public abstract void setInetAddress(java.net.InetAddress inetAddress);

    public abstract org.pircbotx.DccManager getDccManager();

    public abstract void setWebIrcHostname(java.lang.String webIrcHostname);

    public abstract java.lang.String getWebIrcHostname();

    public abstract void setWebIrcAddress(java.net.InetAddress webIrcAddress);

    public abstract java.net.InetAddress getWebIrcAddress();

    public abstract void setWebIrcPassword(java.lang.String webIrcPassword);

    public abstract java.lang.String getWebIrcPassword();

    public abstract void setAutoSplitMessage(boolean autoSplitMessage);

    public abstract boolean isAutoSplitMessage();*/
}
