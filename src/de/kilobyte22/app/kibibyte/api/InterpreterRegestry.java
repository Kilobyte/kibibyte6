package de.kilobyte22.app.kibibyte.api;

import de.kilobyte22.app.kibibyte.misc.IInterpreter;

import java.util.HashMap;

public class InterpreterRegestry {
    private static HashMap<String, IInterpreter> interpreters = new HashMap<String, IInterpreter>();

    public static void registerInterpreter(String name, IInterpreter interpreter) {
        interpreters.put(name, interpreter);
    }

    public static void unregisterInterpreter(String name) {
        interpreters.remove(name);
    }

    public static IInterpreter resolve(String name) {
        return interpreters.get(name);
    }
}
