package de.kilobyte22.app.kibibyte;

import de.kilobyte22.app.kibibyte.core.Kibibyte;

public class Main {

    public static void main(String[] args) {
	    new Kibibyte(args).run();
    }
}
