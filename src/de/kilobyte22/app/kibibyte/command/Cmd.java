package de.kilobyte22.app.kibibyte.command;

import de.kilobyte22.app.kibibyte.plugin.BotAccess;

import java.lang.reflect.Method;

public class Cmd {
    private final String command;
    private BotAccess access;
    private final Method method;

    public Cmd(String command, BotAccess access, Method method) {

        this.command = command;
        this.access = access;
        this.method = method;
    }

    public String getCommand() {
        return command;
    }

    public Method getMethod() {
        return method;
    }

    public BotAccess getBotAccess() {
        return access;
    }
}
