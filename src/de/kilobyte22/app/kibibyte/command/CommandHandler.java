package de.kilobyte22.app.kibibyte.command;

import de.kilobyte22.app.kibibyte.core.Kibibyte;
import de.kilobyte22.app.kibibyte.misc.Args;
import de.kilobyte22.app.kibibyte.plugin.BotAccess;
import de.kilobyte22.lib.logging.Logger;
import org.pircbotx.Channel;
import org.pircbotx.User;

/**
 * Created with IntelliJ IDEA.
 * User: Stephan
 * Date: 15.02.13
 * Time: 21:32
 *
 * @author ${user}
 * @copyright Copyright ${year} ${user}
 */
public abstract class CommandHandler {
    //protected CommandOutputStream out;

    protected void print(String message, String... args) {
        botAccess.bot.sendNotice(sender.getNick(), String.format(message, args));
    }

    protected void printChan(String message, String... args) {
        if (channel != null)
            botAccess.bot.sendMessage(channel, String.format(message, args));
        else
            botAccess.bot.sendNotice(sender, String.format(message, args));
    }

    protected Logger logger;

    protected Channel channel;
    protected User sender;
    protected Args args;
    @Deprecated protected Kibibyte bot;
    protected BotAccess botAccess;
    private String command;

    public static final char BOLD = (char) 2;
    public static final char RESET = (char) 15;
    public static final char COLOR = (char) 3;

    //protected void log(String s) {
        //bot.sendMessage(bot.logChan, s);
    //}

    public void setData(Kibibyte b, Channel c, User s, Args a, String cmd) {
        channel = c;
        sender = s;
        args = a;
        bot = b;
        command = cmd;
        logger = new Logger("CMD:" + cmd);
    }

    @Deprecated
    public void setData(Kibibyte k, BotAccess b, Channel c, User s, Args a, String cmd) {
        channel = c;
        sender = s;
        args = a;
        botAccess = b;
        bot = k;
        command = cmd;
        logger = new Logger("CMD:" + cmd);
    }

    public void setData(BotAccess b, Channel c, User s, Args a, String cmd) {
        channel = c;
        sender = s;
        args = a;
        botAccess = b;
        command = cmd;
        logger = new Logger("CMD:" + cmd);
    }
}
