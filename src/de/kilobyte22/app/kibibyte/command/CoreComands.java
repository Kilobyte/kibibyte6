package de.kilobyte22.app.kibibyte.command;

import de.kilobyte22.app.kibibyte.core.Kibibyte;
import de.kilobyte22.app.kibibyte.event.ShutdownEvent;
import de.kilobyte22.app.kibibyte.exceptions.CommandException;
import de.kilobyte22.app.kibibyte.exceptions.InvalidPluginException;
import de.kilobyte22.app.kibibyte.exceptions.RehashException;
import de.kilobyte22.app.kibibyte.exceptions.UsageException;
import de.kilobyte22.app.kibibyte.misc.AccessLevel;
import de.kilobyte22.app.kibibyte.misc.PermissionSystem;

import java.lang.reflect.Method;
import java.util.List;

public final class CoreComands extends CommandHandler {

    @Override
    protected void print(String message, String... args) {
        bot.sendNotice(sender, String.format(message, args));
    }
    @Override
    protected void printChan(String message, String... args) {
        if (channel != null)
            bot.sendMessage(channel, String.format(message, args));
        else
            bot.sendNotice(sender, String.format(message, args));
    }

    @Command(name = "test", usage = "<text>", help = "This is for testing", extendedCharArgs = "pr", extendedLongArgs = {"prefix", "redirect"}, permission = "debug.test")
	    public void test() {
	        String prefix = args.getNamedParam('p');
	        if (prefix == null) prefix = args.getNamedParam("prefix");
	        if (prefix == null) prefix = "Test:";
	        String redirect = args.getNamedParam('r');
	        if (redirect == null) redirect = args.getNamedParam("redirect");
	        if (redirect == null) redirect = "notice";
	        if (redirect.equalsIgnoreCase("notice"))
	            print(prefix + " " + args.getOrError(1));
	        else if (redirect.equalsIgnoreCase("channel"))
	            printChan(prefix + " " + args.getOrError(1));
	        //out.printLine(prefix + " " + args.getOrError(1));
    }

    @Command(name = "permtest", help = "tests permissions", usage = "", permission = "test")
    public void permtest() {
        print("YAY");
    }

    @Command(name = "info", help = "prints info on the bot", usage = "")
    public void info() {
        print("Bot: Kibibyte v%s; Owner: %s", Kibibyte.VERSION, bot.getOwner());
    }

    @Command(name = "setrequiredlevel", usage = "(none|voice|halfop|op|superop|owner)", help = "Sets the required mode for botuse in a channel", permission = "core.setrequiredlevel")
    public void setreqlevel() {
        if (channel == null)
            throw new CommandException("This command has to be used in a channel");
        AccessLevel lvl = AccessLevel.fromName(args.getOrError(1));
        if (lvl == null)
            throw new CommandException("Unknown level");
        bot.permissionSystem.setUseLevel(channel, lvl);
        print("Done.");
    }

    @Command(name = "load", help = "Loads a plugin", usage = "[-c/--class <class>] <name>", extendedLongArgs = {"class"}, extendedCharArgs = "c", permission = "plugin.load")
    public void load() {
        if (bot.pluginSystem.isLoaded(args.getOrError(1))) {
            throw new CommandException("Plugin already loaded");
        }
        String clazz = args.getNamedParam("class", 'c');
        if (clazz != null) {
            try {
                bot.pluginSystem.loadPluginByClass(clazz, args.getOrError(1));
                bot.pluginSystem.enable(args.getOrError(1));
                print("Done.");
            } catch (ClassNotFoundException e) {
                throw new CommandException("Invalid Plugin Class");
            } catch (InvalidPluginException e) {
                throw new CommandException("Invalid Plugin: " + e.getMessage());
            } catch (InstantiationException e) {
                logger.log(e);
                throw new CommandException("Could not create new Plugin instance. See console");
            }
        } else {
            try {
                if (!bot.pluginSystem.pluginExists(args.getOrError(1))) {
                    throw new CommandException("Plugin does not exist");
                }
                bot.pluginSystem.loadPluginByName(args.getOrError(1));
                bot.pluginSystem.enable(args.getOrError(1));
                print("Done.");
            } catch (InvalidPluginException e) {
                throw new CommandException("Invalid Plugin: " + e.getMessage());
            } catch (InstantiationException e) {
                logger.log(e);
                throw new CommandException("Could not create new Plugin instance. See console");
            }
        }
    }

    @Command(name = "enable", help = "Enables a plugin", permission = "plugin.enable", usage = "<name>")
    public void enable() {
        if (!bot.pluginSystem.isLoaded(args.getOrError(1))) {
            throw new CommandException("Plugin not loaded");
        }
        bot.pluginSystem.enable(args.getOrError(1));
        print("Done.");
    }

    @Command(name = "disable", help = "Disables a plugin", permission = "plugin.disable", usage = "<name>")
    public void disable() {
        if (!bot.pluginSystem.isLoaded(args.getOrError(1))) {
            throw new CommandException("Plugin not loaded");
        }
        bot.pluginSystem.disable(args.getOrError(1));
        print("Done.");
    }

    @Command(name = "help", usage = "[<command>]", help = "Gets help for a command", permission = "core.help")
    public void help() {
        String cmd = args.get(1);
        if (cmd == null) {
            List<Cmd> commands = bot.commandManager.getCommands();
            String tmp = "";
            for (Cmd m : commands) {
                Command c = ((Command)m.getMethod().getAnnotation(Command.class));
                if (bot.permissionSystem.hasParsedPermission(c.permission(), sender, channel)) {
                    if (!tmp.equals("")) tmp += ", ";
                    tmp += c.name();
                }
            }
            print("Avaible commands: " + tmp);
            tmp = "";
            for (String s : bot.prefixes) {
                if (!tmp.equals("")) tmp += ", ";
                tmp += s;
            }
            print("Valid prefixes: " + tmp);
        } else{
            Cmd m = bot.commandManager.getCommand(cmd);
            if (m == null)
                throw new CommandException("Unknown command");
            Command c = ((Command)m.getMethod().getAnnotation(Command.class));
            if (!bot.permissionSystem.hasParsedPermission(c.permission(), sender, channel))
                throw new CommandException("You have no permissions for that command");
            print("Usage: " + c.name() + " " + c.usage());
            print("Aliases: " + c.aliases().toString());
            print(c.help());
        }
    }

    @Command(name = "group", usage = "(create|delete|adduser|removeuser) <group> [<params>]", help = "Manages groups", permission = "admin.group")
    public void group() {
        String mode = args.getOrError(1);
        String name = args.getOrError(2);
        if (mode.equalsIgnoreCase("create")) {
            if (bot.permissionSystem.groupExists(name))
                throw new CommandException("Group already exists");
            bot.permissionSystem.createGroup(name);
        } else if (mode.equalsIgnoreCase("delete")) {
            if (!bot.permissionSystem.groupExists(name))
                throw new CommandException("Group does not exist");
            bot.permissionSystem.deleteGroup(name);
        } else {
            throw new UsageException();
        }
        print("Done.");
    }

    @Command(name = "perm", usage = "[-g/--group] (give|take|reset|has) <target> <permission>", help = "Permission administraction command", permission = "admin.perm")
    public void perm() {
        PermissionSystem ps = bot.permissionSystem;
        String target = args.getOrError(2), permission = args.getOrError(3);
        Boolean value = null;
        boolean has = false;
        if (args.getOrError(1).equalsIgnoreCase("give")) {
            value = true;
        } else if (args.getOrError(1).equalsIgnoreCase("take")) {
            value = false;
        } else if (args.getOrError(1).equalsIgnoreCase("reset")) {

        } else if(args.getOrError(1).equalsIgnoreCase("has")) {
            has = true;
        } else
            throw new CommandException("Invalid mode");
        if (args.getNamedParam("group", 'g') != null) {
            if (!ps.groupExists(target))
                throw new CommandException("Invalid Group");
            if (has) {
                Boolean p = ps.getHandler().getGroup(target).hasPermission(permission);
                print("Group " + target + (p != null ? (p ? " has that permission" : "doesn't have that permission") : " has no entry for that permission") + " directly set");
                return;
            }
            else
                ps.setGroupPermission(target, permission, value);
        } else {
            if (has) {
                Boolean p = ps.getHandler().getPermission(permission, target);
                print(target + (p != null ? (p ? " has that permission" : "doesn't have that permission") : " has no entry for that permission") + " directly set");
                return;
            } else
                ps.setUserPermission(target, permission, value);
        }
        print("Done.");
    }



    @Command(name = "restart", usage = "",  aliases = {"relog"}, help = "disconnects and reconnects the bot", permission = "core.reconnect")
    public void reconnect() {
        try {
            // TODO: Make it rejoin channels
            /*Set<Channel> channels = bot.getChannels();
            String channellist = "";
            for (Channel c : channels) {

            }
            bot.quitServer(sender.getNick() + " is restarting bot...");*/
            bot.restart(sender.getNick() + " is restarting bot...");
        } catch (Exception e) {
            logger.log(e);
        }
    }

    @Command(name = "shutdown", aliases = {"exit"}, usage = "[reason]", help = "Stops the bot", permission = "core.admin.shutdown")
    public void shutdown() {
        String msg = args.merge(1, 0);
        if (msg.equals("")) msg = "Kibibyte v" + Kibibyte.VERSION + " terminated by " + sender.getNick();
        else msg = "[" + sender.getNick() + "] " + msg;
        bot.setAutoReconnect(false);
        bot.quitServer(msg);
        bot.eventBus.post(new ShutdownEvent());
        System.exit(0);
    }

    @Command(name = "rehash", usage = "", help = "Reloads the config", permission = "core.rehash")
    public void rehash() {
        try {
            bot.rehash();
            print("Done.");
        } catch (RehashException e) {
            logger.log(e);
            throw new CommandException("Rehash failed: " + e.getMessage());
        }
    }

    @Command(name = "listplugins", usage = "[-l/--loaded|-u/--unloaded][-e/--enabled|-d|--disabled]", help = "Lists all plugins. -l or -u limit output to just loaded/unloaded plugins", permission = "plugin.list")
    public void listplugins() {
        boolean l = args.getNamedParam("loaded", 'l') != null,
                u = args.getNamedParam("unloaded", 'u') != null,
                d = args.getNamedParam("disabled", 'd') != null,
                e = args.getNamedParam("enabled", 'e') != null;
        // check error cases
        if (l && u)
            throw new CommandException("-u and -l exclude each other, but are both set");
        if (u && (d || e))
            throw new CommandException("-e and -d make no sense when -u is set.");
        // set some defaults
        if (!(d || e))
            d = e = true;
        if (!(l || u))
            l = u = true;
        String out = "";
        for (String s : bot.pluginSystem.listPlugins()) {
            if (bot.pluginSystem.isLoaded(s)) {
                if (l) {
                    if (bot.pluginSystem.isEnabled(s)) {
                        if (e) {
                            if (!out.equals("")) out += ", ";
                            out += COLOR + "3" + s + RESET;
                        }
                    } else {
                        if (d) {
                            if (!out.equals("")) out += ", ";
                            out += COLOR + "4" + s + RESET;
                        }
                    }
                }
            } else if (d) {
                if (!out.equals("")) out += ", ";
                out += COLOR + "2" + s + RESET;
            }
        }

        print((out.length() > 0 ? "A" : "No a") + "viable plugins" + (out.length() > 0 ? ": " + out : "."));
    }
}
