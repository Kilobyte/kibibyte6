package de.kilobyte22.app.kibibyte.command;

import com.google.common.eventbus.Subscribe;
import de.kilobyte22.app.kibibyte.core.Kibibyte;
import de.kilobyte22.app.kibibyte.event.CommandNotFoundEvent;
import de.kilobyte22.app.kibibyte.event.NonCommandMessageEvent;
import de.kilobyte22.app.kibibyte.exceptions.CommandException;
import de.kilobyte22.app.kibibyte.exceptions.UsageException;
import de.kilobyte22.app.kibibyte.misc.Args;
import de.kilobyte22.app.kibibyte.misc.Utils;
import de.kilobyte22.app.kibibyte.plugin.BotAccess;
import de.kilobyte22.lib.logging.LogLevel;
import de.kilobyte22.lib.logging.Logger;
import org.pircbotx.Channel;
import org.pircbotx.User;
import org.pircbotx.hooks.events.MessageEvent;
import org.pircbotx.hooks.events.NoticeEvent;
import org.pircbotx.hooks.events.PrivateMessageEvent;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Stephan
 * Date: 15.02.13
 * Time: 15:09
 *
 * @author ${user}
 * @copyright Copyright ${year} ${user}
 */
public class CommandManager {
    Kibibyte bot;
    LinkedList<CommandManager> childs = new LinkedList<CommandManager>();

    HashMap<String, Cmd> commands = new HashMap<String, Cmd>();
    private Logger logger = new Logger("COMMANDSYS");
    private CommandManager parent;
    private boolean isEnabled = true;
    private BotAccess botAccess;

    public CommandManager(Kibibyte bot) {
        this.bot = bot;
        bot.eventBus.register(this);
    }

    public CommandManager(Kibibyte bot, CommandManager parent) {
        this.bot = bot;
        this.parent = parent;
        parent.childs.add(this);
    }

    public CommandManager(Kibibyte bot, CommandManager parent, BotAccess botAccess) {
        this(bot, parent);
        this.botAccess = botAccess;
        this.logger = new Logger("PLUGIN/" + botAccess.getPluginName() + "/COMMANDSYS");
    }

    public CommandManager(CommandManager parent, BotAccess botAccess) {
        this(parent.bot, parent, botAccess);
    }

    public CommandManager(CommandManager parent) {
        this(parent.bot, parent);
    }

    @Subscribe
    public void onMessage(MessageEvent event) {
        if (parent != null) return;
        String msg = Utils.getMessageWithoutPrefix(bot, event.getChannel().getName(), event.getMessage());
        if (msg != null) {
            //ChannelOutput out = new ChannelOutput();
            //Bot bot = this.bot.botLookup.get(event.getBot());
            //out.setDetails(event.getChannel(), event.getUser(), bot);
            executeLine(msg, event.getChannel(), event.getUser(), bot);
            bot.nickservSystem.flushAccount(event.getUser().getNick());
        } else {
            bot.eventBus.post(new NonCommandMessageEvent(event));
        }
    }

    @Subscribe
    public void onPrivateMessage(PrivateMessageEvent event) {
        if (parent != null) return;
        if (event.getUser().getHostmask().equals("")) return;
        executeLine(event.getMessage(), null, event.getUser(), bot);
        bot.nickservSystem.flushAccount(event.getUser().getNick());
    }

    @Subscribe
    public void onNotice(NoticeEvent event) {
        if (parent != null) return;
        if (event.getUser().getHostmask().equals("")) return;
        executeLine(event.getMessage(), null, event.getUser(), bot);
        bot.nickservSystem.flushAccount(event.getUser().getNick());
    }

    private void executeLine(String msg, Channel channel, User sender, Kibibyte bot) {
        if (channel != null && (!bot.permissionSystem.mayUse(sender, channel)))
            return;
        // parsing comes later
        parseAndRun(msg, channel, sender, bot);
    }

    private void parseAndRun(String msg, Channel channel, User sender, Kibibyte bot) {
        logger.log(LogLevel.DEBUG, "Executing...");
        String cmdname = msg.split(" ")[0];
        logger.log(LogLevel.DEBUG, "Name: " + cmdname);
        Cmd cmd = getCommand(cmdname);
        if (cmd != null) {
            Class<?extends CommandHandler> handlerClass = (Class<? extends CommandHandler>) cmd.getMethod().getDeclaringClass();
            if (!bot.permissionSystem.hasParsedPermission(cmd.getMethod().getAnnotation(Command.class).permission(),sender, channel)) {
                bot.sendNotice(sender, "You don't have permissions to execute this command");
                return;
            }

            try {
                //"(\"(.*[^\\\\]|)\"|[^ ]+)"  msg.substring(cmdname.length() + 1)
                String argstring;
                Command annotation = cmd.getMethod().getAnnotation(Command.class);
                try {
                    argstring = msg.substring(cmdname.length() + 1);
                } catch (Exception ex) {
                    argstring = "";
                }
                Args args_ = Utils.parseArgs(argstring, annotation.extendedCharArgs().toCharArray(), annotation.extendedLongArgs());

                //logger.log(LogLevel.DEBUG, args_.toString());
                CommandHandler handler = handlerClass.newInstance();
                if (cmd.getBotAccess() == null)
                    handler.setData(bot, channel, sender, args_, cmd.getMethod().getAnnotation(Command.class).name());
                else
                    handler.setData(cmd.getBotAccess(), channel, sender, args_, cmd.getMethod().getAnnotation(Command.class).name());
                try {
                    cmd.getMethod().invoke(handler);
                } catch (InvocationTargetException ex) {
                    try {
                        throw ex.getCause();
                    } catch (UsageException ex_) {
                        logger.log(annotation.toString());
                        logger.log(bot.toString());
                        bot.sendNotice(sender, "Usage: " + cmdname + " " + annotation.usage());
                    } catch (CommandException ex_) {
                        bot.sendNotice(sender, "Error: " + ex_.getMessage());
                    } catch (Throwable throwable) {
                        logger.log(throwable);
                    }
                }
            } catch (Exception ex) {
                logger.log(ex);
            }
        } else {
            bot.eventBus.post(new CommandNotFoundEvent(cmdname, sender, channel, msg));
            if (channel == null)
                bot.sendNotice(sender, "Invalid command");
            //logger.log(LogLevel.DEBUG, "But it was not found :(");
        }
        bot.nickservSystem.flushAccount(sender.getNick());
    }

    public void unregisterHandler(Class<?extends CommandHandler> handler) {
        for(Method m : handler.getMethods()) {
            Command annotation = m.getAnnotation(Command.class);
            if (annotation != null) {
                if (commands.get(annotation.name()).getMethod() == m)
                    commands.remove(annotation.name());
                for (String s : annotation.aliases()) {
                    if(commands.get(s).getMethod() == m)
                        commands.remove(s);
                }
            }
        }
    }

    public void registerHandler(Class<?extends CommandHandler> handler) {
        for(Method m : handler.getMethods()) {
            Command annotation = m.getAnnotation(Command.class);
            logger.log(LogLevel.DEBUG, "Found method: " + m.getName());
            if (annotation != null) {
                commands.put(annotation.name(), new Cmd(annotation.name(), botAccess, m));
                logger.log(LogLevel.DEBUG, "Found Command");
                for (String s : annotation.aliases()) {
                    commands.put(s, new Cmd(annotation.name(), botAccess, m));
                }
            }
        }
    }

    public Cmd getCommand(String name) {
        return getCommand(name, null);
    }

    private Cmd getCommand(String name, CommandManager sender) {
        if (!isEnabled) {
            logger.log(LogLevel.DEBUG, "Not enabled, ignoring command request");
            return null;
        }
        Cmd m = commands.get(name);
        if (m != null) return m;
        for (CommandManager child : childs) {
            if (child != sender) {
                logger.log(LogLevel.DEBUG, "Asking child...");
                m = child.getCommand(name, this);
                if (m != null) {
                    logger.log(LogLevel.DEBUG, "Found command");
                    return m;
                }
            }
        }
        if (parent != null && sender != parent) {
            logger.log("Didn't find, asking parent");
            return parent.getCommand(name, this);
        }
        logger.log("Didn't find");
        return null;
    }

    public List<Cmd> getCommands(CommandManager sender) {
        List<Cmd> ret = new LinkedList<Cmd>();
        if (!isEnabled) return ret;
        for (String s : commands.keySet()) {
            if (commands.get(s).getCommand().equals(s))
                ret.add(commands.get(s));
        }
        if (parent != null && sender != parent)
            for (Cmd m : parent.getCommands(this)) {
                ret.add(m);
            }
        for (CommandManager child : childs) {
            if (child.isEnabled && child != sender)
                for (Cmd m : child.getCommands(this)) {
                    ret.add(m);
                }
        }
        return  ret;
    }

    public void unlink() {
        parent.childs.remove(this);
        this.childs = new LinkedList<CommandManager>();
    }

    public void setEnabled(boolean state) {
        isEnabled = state;
    }

    public List<Cmd> getCommands() {
        return getCommands(null);
    }
}