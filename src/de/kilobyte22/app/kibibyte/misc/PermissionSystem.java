package de.kilobyte22.app.kibibyte.misc;

import de.kilobyte22.app.kibibyte.api.permission.IGroup;
import de.kilobyte22.app.kibibyte.api.permission.IPermissionHandler;
import de.kilobyte22.app.kibibyte.api.permission.ISpecialPermissionHandler;
import de.kilobyte22.app.kibibyte.api.permission.PermissionRegistry;
import de.kilobyte22.app.kibibyte.core.Kibibyte;
import de.kilobyte22.lib.logging.LogLevel;
import de.kilobyte22.lib.logging.Logger;
import org.pircbotx.Channel;
import org.pircbotx.User;

import java.util.LinkedList;
import java.util.Stack;

public class PermissionSystem {
    private IPermissionHandler handler;
    private Kibibyte bot;
    private Logger logger = new Logger("PERMISSIONSYS");

    public PermissionSystem(Kibibyte bot) {
        this.bot = bot;
    }

    public boolean hasParsedPermission(String permission, User user, Channel channel) {
        if (permission == null || permission.equals(""))
            return true;
        String nsnick = bot.nickservSystem.getAccount(user.getNick());
        //new PermissionTermInterpreter(permission);
        return hasPermission(permission, user, channel, nsnick);
    }

    private Boolean userHasPermission_(String nick, String permission) {
        Boolean hasPerm = userHasPermission(nick, permission);
        if (hasPerm != null) return hasPerm;
        for (IGroup group : handler.getGroups(nick)) {
            if (group == null) {
                logger.log(LogLevel.WARNING, "The permission handler author derped as in returning a null group. Skipping it.");
            } else {
                hasPerm = groupHasPermission(group, permission);
                if (hasPerm) return true;
            }
        }
        return null;
    }

    public boolean hasPermission(String permission, User user, Channel channel, String nsnick) {
        return hasPermission(permission, user, channel, nsnick, false);
    }

    public boolean hasPermission(String permission, User user, Channel channel, String nsnick, Boolean def) {
        Boolean hasPerm = userHasPermission_(nsnick, permission);
        if (hasPerm != null) return hasPerm;
        Boolean hasNoPerm = false;
        for (IGroup group : handler.getGroups(nsnick)) {
            hasPerm = groupHasPermission(group, permission);
            if (hasPerm != null && hasPerm) return true;
            if (hasPerm != null && !hasPerm) hasNoPerm = true;
        }
        if (hasNoPerm) return false;
        LinkedList<String> sgroups = new LinkedList<String>();
        for (ISpecialPermissionHandler h : PermissionRegistry.getSpecialPermissionHandlers()) {
            /*for (IGroup group : h.getSpecialGroups(user, channel)) {
                hasPerm = group.hasPermission(permission);
                if (hasPerm) return true;
            }*/
            sgroups.addAll(h.getSpecialGroups(user, channel));
        }
        for (String s : sgroups) {
            if (groupExists(s)) {
                IGroup group = handler.getGroup(s);
                hasPerm = group.hasPermission(permission);
                if (hasPerm != null && hasPerm) return true;
                if (hasPerm != null && !hasPerm) hasNoPerm = true;
            }
        }
        if (hasNoPerm) return false;
        hasPerm = userHasPermission_("n:" + user.getNick(), permission);
        if (hasPerm != null) return hasPerm;
        hasPerm = userHasPermission_(":default", permission);
        if (hasPerm != null) return hasPerm;
        return def;
    }

    public Boolean mayUse(User user, Channel channel) {
        return handler.mayUse(user, channel);
    }

    public void setUseLevel(Channel channel, AccessLevel accessLevel) {
        handler.setUseLevel(channel, accessLevel);
    }

    private Boolean groupHasPermission(IGroup group, String permission) {
        String node = permission;
        do {
            String realnode = node + ((permission.equalsIgnoreCase(node) || node.equals("*")) ? "" : ".*");
            Boolean p = group.hasPermission(realnode);
            if (p != null) return p;
            String[] nodehacked = node.split("\\.");
            try {
                node = node.substring(0, node.length() - nodehacked[nodehacked.length - 1].length() - 1);
            } catch (Exception ex) {
                node = (node.equals("*") ? "" : "*");
            }
        } while (!( node.equals("")));
        IGroup parent = group.getParent();
        if (parent == null)
            return null;
        return groupHasPermission(parent, permission);
    }

    private Boolean userHasPermission(String user, String permission) {
        if (user == null) return null;
        String node = permission;
        do {
            String realnode = node + ((permission.equalsIgnoreCase(node) || node.equals("*")) ? "" : ".*");
            Boolean p = handler.getPermission(realnode, user);
            if (p != null) return p;
            String[] nodehacked = node.split("\\.");
            try {
                node = node.substring(0, node.length() - nodehacked[nodehacked.length - 1].length() - 1);
            } catch (Exception ex) {
                node = (node.equals("*") ? "" : "*");
            }
        } while (!( node.equals("")));
        return null;
    }

    public void setUserPermission(String user, String permission, Boolean value) {
        handler.setUserPermission(user, permission, value);
    }

    public boolean groupExists(String name) {
        return handler.getGroup(name) != null;
    }

    public void setGroupPermission(String group, String permission, Boolean value) {
        handler.getGroup(group).setPermission(permission, value);
    }

    public IPermissionHandler getHandler() {
        return handler;
    }

    public void setHandler(IPermissionHandler handler) {
        this.handler = handler;
    }

    public void createGroup(String name) {
        handler.createGroup(name);
    }

    public void deleteGroup(String name) {
        handler.deleteGroup(name);
    }

    private class PermissionTermInterpreter {
        private final String[] term;
        private int idx = 0;
        private Stack<String> oStack = new Stack<String>(), pStack = new Stack<String>();

        public PermissionTermInterpreter(String term) {
            this.term = term.split(" ");
        }


        public boolean interpret() {
            for (idx = 0; idx < term.length; idx++) {
                boolean found = false;
                if (term[idx].length() == 1) {
                    switch (term[idx].charAt(0)) {
                        case '&':case '|':case'(':

                    }
                }
                if (!found) {
                    pStack.push(term[idx]);
                }
            }
            return pStack.pop().equals("true");
        }
    }
}
