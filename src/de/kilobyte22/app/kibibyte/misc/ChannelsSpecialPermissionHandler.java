package de.kilobyte22.app.kibibyte.misc;

import de.kilobyte22.app.kibibyte.api.permission.ISpecialPermissionHandler;
import org.pircbotx.Channel;
import org.pircbotx.User;

import java.util.LinkedList;
import java.util.List;

public class ChannelsSpecialPermissionHandler implements ISpecialPermissionHandler {
    @Override
    public List<String> getSpecialGroups(User user, Channel channel) {
        List<String> groups = new LinkedList<String>();
        if (channel.getVoices().contains(user))
            groups.add("special.channel.voice");
        if (channel.getHalfOps().contains(user))
            groups.add("special.channel.halfop");
        if (channel.getOps().contains(user))
            groups.add("special.channel.op");
        if (channel.getSuperOps().contains(user))
            groups.add("special.channel.superop");
        if (channel.getOwners().contains(user))
            groups.add("special.channel.owner");
        return groups;
    }
}
