package de.kilobyte22.app.kibibyte.misc;

import java.util.Date;

public class TimeUtils {
    public static long getCurrentTimeStamp() {
        return System.currentTimeMillis()/1000;
    }

    public static long timestampFromDate(Date date) {
        return date.getTime() / 1000;
    }

    public static Date dateFromTimeStamp(long timestamp) {
        return new Date(timestamp * 1000);
    }
}
