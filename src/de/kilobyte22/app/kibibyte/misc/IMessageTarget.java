package de.kilobyte22.app.kibibyte.misc;

public interface IMessageTarget {
    void print(String message);
}
