package de.kilobyte22.app.kibibyte.misc;

import java.util.HashMap;

public enum AccessLevel {
    ALL (0, "all"),
    VOICE (1, "voice"),
    HALFOP (2, "halfop"),
    OP (3, "op"),
    SUPEROP (4, "superop"),
    OWNER (5, "owner");

    private final int id;
    private final String name;
    private static HashMap<String, AccessLevel> lookup = new HashMap<String, AccessLevel>();
    private static AccessLevel[] idlookup = new AccessLevel[]{ALL, VOICE, HALFOP, OP, SUPEROP, OWNER};
    static {
        for (AccessLevel al : idlookup) {
            lookup.put(al.getName(), al);
        }
    }

    AccessLevel(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public static AccessLevel fromID(int id) {
        //switch(id) {
            /*case 0: return ALL;
            case 1: return VOICE;
            case 2: return HALFOP;
            case 3: return OP;
            case 4: return SUPEROP;
            case 5: return OWNER;
            default: return null;*/
        //}
        return idlookup[id];
    }

    public int getID() {
        return id;
    }

    public String getName() {
        return name;
    }

    public static AccessLevel fromName(String name) {
        return lookup.get(name.toLowerCase());
    }
}
