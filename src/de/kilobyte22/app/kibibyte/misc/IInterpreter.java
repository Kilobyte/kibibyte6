package de.kilobyte22.app.kibibyte.misc;

import java.util.List;

public interface IInterpreter {
    public void run(String code, List<String> args, IMessageTarget target, ICommandSender sender);
}
