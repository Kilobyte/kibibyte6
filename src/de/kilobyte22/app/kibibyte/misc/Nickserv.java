package de.kilobyte22.app.kibibyte.misc;

import com.google.common.eventbus.Subscribe;
import de.kilobyte22.app.kibibyte.core.Kibibyte;
import org.pircbotx.hooks.events.NoticeEvent;

import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * User: Stephan
 * Date: 17.02.13
 * Time: 15:08
 *
 * @author Stephan
 * @copyright Copyright 2013 Stephan
 */
public class Nickserv {
    Kibibyte bot;
    HashMap<String, Thread> getters = new HashMap<String, Thread>();
    HashMap<String, String> accounts = new HashMap<String, String>();
    public Nickserv(Kibibyte b) {
        bot = b;
        bot.eventBus.register(this);
    }

    public String getAccount(String nick) {
        if (accounts.get(nick) != null) {
            if (accounts.get(nick).equals(""))
                return null;
            else
                return accounts.get(nick);
        }
        getters.put(nick, Thread.currentThread());
        bot.sendMessage("Nickserv", "ACC " + nick + " *");
        try {
            Thread.sleep(1000);
            accounts.put(nick, "");
            return null;
        } catch (InterruptedException e) {
            return accounts.get(nick);
        }
    }

    public void flushAccount(String nick) {
        if (accounts.get(nick) != null)
            accounts.remove(nick);
    }

    @Subscribe
    public void onNotice(NoticeEvent event) {
        if (event.getBot() == bot && event.getUser().getNick().equalsIgnoreCase("nickserv")) {
            String stuff[] = event.getMessage().split(" ");
            if (stuff[3].equals("ACC") && stuff[1].equals("->") && stuff[4].equals("3")) {
                accounts.put(stuff[0], stuff[2]);
                getters.get(stuff[0]).interrupt();
            }
        }

    }
}
