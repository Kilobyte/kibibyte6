package de.kilobyte22.app.kibibyte.misc;

public interface ICommandSender {
    public String getName();
    public String sendMessage(String message);
}
