package de.kilobyte22.app.kibibyte.misc;

import com.google.common.eventbus.Subscribe;
import de.kilobyte22.app.kibibyte.api.permission.IGroup;
import de.kilobyte22.app.kibibyte.api.permission.IPermissionHandler;
import de.kilobyte22.app.kibibyte.core.Kibibyte;
import de.kilobyte22.app.kibibyte.event.RehashEvent;
import de.kilobyte22.lib.configuration.ConfigFile;
import de.kilobyte22.lib.configuration.ConfigMap;
import de.kilobyte22.lib.configuration.ConfigNode;
import de.kilobyte22.lib.configuration.ConfigString;
import org.pircbotx.Channel;
import org.pircbotx.User;

import java.io.File;
import java.util.HashMap;
import java.util.LinkedList;

public class YamlPermissionHandler implements IPermissionHandler {

    private HashMap<String, IGroup> groups = new HashMap<String, IGroup>();
    private ConfigFile permFile = new ConfigFile(new File("permissions.yml"));
    private ConfigMap users = new ConfigMap();

    public YamlPermissionHandler() {
        onRehash(null);
    }

    @Override
    public Boolean getPermission(String perm, String user) {
        ConfigMap tmp = permFile.getMap("users");
        if (!tmp.containsKey(user))
            return null;
        return tmp.getMap(user).getMap("permissions").getBoolean(perm);

        //return permFile.getMap("users").getMap(user).getMap("permissions").getBoolean(perm);
    }

    @Override
    public LinkedList<IGroup> getGroups(String user) {
        LinkedList<IGroup> ret = new LinkedList<IGroup>();
        for (ConfigNode node : permFile.getMap("users").getMap(user).getList("groups")) {
            ret.add(groups.get(node.toString()));
        }
        return ret;
    }

    @Override
    public void setUserPermission(String user, String permission, Boolean value) {
        ConfigMap perms = permFile.getMap("users").getMap(user).getMap("permissions");
        perms.setBoolean(permission, value);
        permFile.save();
    }

    @Override
    public IGroup getGroup(String name) {
        return groups.get(name);
    }

    private AccessLevel getAccessLevel(User user, Channel channel) {
        if (channel.getOwners().contains(user))
            return AccessLevel.OWNER;
        if (channel.getSuperOps().contains(user))
            return AccessLevel.SUPEROP;
        if (channel.getOps().contains(user))
            return AccessLevel.OP;
        if (channel.getHalfOps().contains(user))
            return AccessLevel.HALFOP;
        if (channel.getVoices().contains(user))
            return AccessLevel.VOICE;
        return AccessLevel.ALL;
    }

    @Override
    public boolean mayUse(User user, Channel channel) {
        ConfigMap als = permFile.getMap("accesslevels");
        AccessLevel al = AccessLevel.fromID(als.getInt(channel.getName(), 0));
        if (al.getID() <= getAccessLevel(user, channel).getID())
            return true;
        return false;
    }

    @Override
    public void setUseLevel(Channel channel, AccessLevel accessLevel) {
        ConfigMap als = permFile.getMap("accesslevels");
        als.put(channel.getName(), accessLevel.getID());
        permFile.save();
    }

    @Override
    public void createGroup(String name) {
        permFile.getMap("groups").getMap(name);
        permFile.save();
        groups.put(name, new Group(name));
    }

    @Override
    public void deleteGroup(String name) {
        permFile.getMap("groups").remove(name);
        permFile.save();
        groups.remove(name);
    }

    @Subscribe
    public void onRehash(RehashEvent event) {
        permFile.load();
        users = permFile.getMap("users");
        ConfigMap grps = permFile.getMap("groups");
        groups.clear();
        for (String name : grps.keySet()) {
            groups.put(name, new Group(name));
        }
        permFile.save();
    }

    private class Group implements IGroup {
        private String name;
        private IGroup parent = null;

        public Group(String name) {
            this.name = name;
        }

        @Override
        public Boolean hasPermission(String permission) {
            return permFile.getMap("groups").getMap(name).getMap("permissions").getBoolean(permission);
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public IGroup getParent() {
            String pname = permFile.getMap("groups").getMap(name).getString("parent");
            if (pname == null) return null;
            return groups.get(pname);
        }

        @Override
        public void setParent(IGroup group) {
            parent = group;
        }

        @Override
        public void setPermission(String permission, Boolean value) {
            permFile.getMap("groups").getMap(name).getMap("permissions").setBoolean(permission, value);
            permFile.save();
        }

        @Override
        public void addUser(String user) {
            permFile.getMap("users").getMap(user).getList("groups").add(new ConfigString(this.name));
            permFile.save();
        }

        @Override
        public void removeUser(String user) {
            permFile.getMap("users").getMap(user).getList("groups").removeObject(name);
            permFile.save();
        }
    }
}
