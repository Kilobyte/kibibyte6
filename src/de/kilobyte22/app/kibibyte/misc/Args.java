package de.kilobyte22.app.kibibyte.misc;


import de.kilobyte22.app.kibibyte.exceptions.UsageException;

import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * User: Stephan
 * Date: 16.02.13
 * Time: 12:23
 *
 * @author ${user}
 * @copyright Copyright ${year} ${user}
 */
public class Args {
    public Args(HashMap<Object, String> args) {
        this.args = args;
    }
    HashMap<Object, String> args;

    public String getOrError(int param) {
        String ret = args.get(param);
        if (ret == null)
            throw new UsageException();
        return ret;
    }

    public String get(int param) {
        return args.get(param);
    }

    public String getNamedParam(String param, char param2) {
        String ret = getNamedParam(param);
        if (ret != null) return ret;
        return getNamedParam(param2);
    }

    public String getNamedParam(String param) {
        return args.get("--" + param);
    }

    public String getNamedParam(char param) {
        return args.get("-" + param);
    }

    public String merge(int start, int end) {
        if (end == 0) end = getLength();
        String ret = "";
        for (int i = start; i <= end; i++) {
            if (i != start) ret += " ";
            ret += args.get(i);
        }
        return ret;
    }

    public int getLength() {
        int l = 0;
        for (Object o : args.keySet()) {
            if (o instanceof Integer && ((Integer) o ) > l) l = (Integer) o;
        }
        return l;
    }

    public int getIntOrError(int param) {
        try {
            return Integer.valueOf(getOrError(param));
        } catch (Exception ex) {
            throw new UsageException();
        }
    }
}
