package de.kilobyte22.app.kibibyte.misc;

import de.kilobyte22.app.kibibyte.core.Kibibyte;
import org.pircbotx.PircBotX;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: Stephan
 * Date: 15.02.13
 * Time: 14:41
 *
 * @author ${user}
 * @copyright Copyright ${year} ${user}
 */
public final class Utils {
    private Utils() {}
    public static HashMap<String, String> parseArgs(HashMap<String, String> aliases) {

        return aliases;
    }

    public static void sleep(int i) {
        try {
            Thread.sleep(i);
        } catch (Exception ex) {}
    }

    public static String getMessageWithoutPrefix(PircBotX bot, String channel, String message) {
        for (String prefix : ((Kibibyte) bot).prefixes) {
            if (message.startsWith(prefix)) {
                return message.substring(prefix.length());
            }
        }
        return null;
    }

    public static List<String> getAllMatches(String of, String on) {
        /*ArrayList<String> ret = new ArrayList<String>();
        Pattern argRegex = Pattern.compile(of);
        Matcher matcher = argRegex.matcher(on);
        int start = 0;
        if (!matcher.matches()) return new String[]{};
        do {
            matcher = argRegex.matcher(on.substring(start));
            int sta = matcher.start();
            int sto = matcher.end();
            ret.add(on.substring(start + sta, start + sto));
            start += sto + 1;
        } while (matcher.matches());
        return (String[]) ret.toArray();*/
        List<String> allMatches = new ArrayList<String>();
        Matcher m = Pattern.compile(of)
                .matcher(on);
        while (m.find()) {
            allMatches.add(m.group());
        }
        return allMatches;
    }

    public static Args parseArgs(String argstring, char[] extendedCharArgs, String[] extendedArgs) {

        LinkedList<Character> xChar = new LinkedList<Character>();
        for (char c : extendedCharArgs) {
            xChar.add(c);
        }
        LinkedList<String> xLong = new LinkedList<String>();
        for (String c : extendedArgs) {
            xLong.add(c);
        }

        List<String> args_2 = /*argstring.split(" "); /*/Utils.getAllMatches("(\"(.*[^\\\\]|)\"|[^ \"][^ ]*)", argstring);
        //logger.log(args_2.toString());
        String[] args = new String[args_2.size()];
        for (int i = 0; i < args_2.size(); i++) {
            args[i] = args_2.get(i).replace("\\*space*", " ");
        }
        HashMap<Object, String> args_ = new HashMap<Object, String>();
        Queue<String> keys = new LinkedList<String>();
        int cnt = 1;
        for (int i = 0; i < args.length; i++) {
            String arg = args[i];
            if (arg.startsWith("--")) {
                //long form
                for(String s : keys) {
                    args_.put(s, "");
                }
                keys.clear();
                if (xLong.contains(arg))
                    keys.offer(arg);
                else
                    args_.put(arg, "");
            } else if (arg.startsWith("-")) {
                for(String s : keys) {
                    args_.put(s, "");
                }
                keys.clear();
                for(int j = 1; j < arg.length(); j++) {
                    if (xChar.contains(arg.charAt(j)))
                        keys.offer("-" + String.valueOf(arg.charAt(j)));
                    else
                        args_.put(arg, "");
                }
            } else {
                if (arg.equals("\"\""))
                    arg = "";
                if (arg.startsWith("\"") && arg.endsWith("\""))
                    arg = arg.substring(1, arg.length() - 1);
                arg = arg.replace("\\\"", "\"");
                String key = keys.poll();
                if (key == null)
                    args_.put(cnt++, arg);
                else
                    args_.put(key, arg);
            }

        }
        return new Args(args_);
    }
}
