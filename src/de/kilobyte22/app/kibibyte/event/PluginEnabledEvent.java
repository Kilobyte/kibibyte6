package de.kilobyte22.app.kibibyte.event;

import de.kilobyte22.app.kibibyte.api.plugin.IPlugin;

public class PluginEnabledEvent extends PluginEvent {
    public PluginEnabledEvent(IPlugin plugin) {
        super(plugin);
    }
}
