package de.kilobyte22.app.kibibyte.event;

import de.kilobyte22.app.kibibyte.api.plugin.IPlugin;

public class PluginUnloadingEvent extends PluginEvent {
    public PluginUnloadingEvent(IPlugin plugin) {
        super(plugin);
    }
}
