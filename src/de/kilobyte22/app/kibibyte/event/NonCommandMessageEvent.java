package de.kilobyte22.app.kibibyte.event;

import org.pircbotx.hooks.events.MessageEvent;

public class NonCommandMessageEvent {

    private final MessageEvent event;

    public NonCommandMessageEvent(MessageEvent event) {

        this.event = event;
    }

    public MessageEvent getOrigEvent() {
        return event;
    }
}
