package de.kilobyte22.app.kibibyte.event;

import de.kilobyte22.app.kibibyte.api.plugin.IPlugin;

public class PluginDisabledEvent extends PluginEvent {
    public PluginDisabledEvent(IPlugin plugin) {
        super(plugin);
    }
}
