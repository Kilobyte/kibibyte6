package de.kilobyte22.app.kibibyte.event;

import org.pircbotx.Channel;
import org.pircbotx.User;

public class CommandNotFoundEvent {
    private final String cmdname;
    private final User sender;
    private final Channel channel;
    private final String msg;

    public CommandNotFoundEvent(String cmdname, User sender, Channel channel, String msg) {
        this.cmdname = cmdname;
        this.sender = sender;
        this.channel = channel;
        this.msg = msg;
    }

    public String getCmdname() {
        return cmdname;
    }

    public User getSender() {
        return sender;
    }

    public Channel getChannel() {
        return channel;
    }

    public String getMsg() {
        return msg;
    }
}
