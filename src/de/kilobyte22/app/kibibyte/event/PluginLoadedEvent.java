package de.kilobyte22.app.kibibyte.event;

import de.kilobyte22.app.kibibyte.api.plugin.IPlugin;

public class PluginLoadedEvent extends PluginEvent {

    public PluginLoadedEvent(IPlugin plugin) {
        super(plugin);
    }
}
