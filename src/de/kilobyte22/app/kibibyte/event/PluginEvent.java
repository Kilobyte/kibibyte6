package de.kilobyte22.app.kibibyte.event;

import de.kilobyte22.app.kibibyte.api.plugin.IPlugin;

public class PluginEvent {
    private final IPlugin plugin;

    public PluginEvent(IPlugin plugin) {
        this.plugin = plugin;
    }

    public IPlugin getPlugin() {
        return plugin;
    }
}
