package de.kilobyte22.app.kibibyte.plugin;

import de.kilobyte22.app.kibibyte.api.permission.IPermissionUser;
import de.kilobyte22.app.kibibyte.core.Kibibyte;
import org.pircbotx.Channel;
import org.pircbotx.User;

public class PermissionUser implements IPermissionUser {

    private User user;

    public PermissionUser(Kibibyte kibibyte, User user) {

        this.user = user;
    }

    @Override
    public boolean hasPermission(String permission, Channel channel) {
        return false;
    }

    @Override
    public boolean hasParsedPermission(String permission, Channel channel) {
        return false;
    }
}
