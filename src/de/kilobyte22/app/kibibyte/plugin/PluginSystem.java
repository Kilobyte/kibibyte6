package de.kilobyte22.app.kibibyte.plugin;

import de.kilobyte22.app.kibibyte.api.plugin.IPlugin;
import de.kilobyte22.app.kibibyte.core.Kibibyte;
import de.kilobyte22.app.kibibyte.exceptions.InvalidPluginException;
import de.kilobyte22.lib.logging.LogLevel;
import de.kilobyte22.lib.logging.Logger;
import org.yaml.snakeyaml.Yaml;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.jar.JarFile;

public class PluginSystem {

    private HashMap<String, Plugin> pluginlist = new HashMap<String, Plugin>();
    private Kibibyte kibibyte;
    private HashMap<String, File> allPlugins;
    private static File pluginDirectory;
    private static Logger logger = new Logger("PLUGINSYS");
    private URLClassLoader loader;
    private boolean initialLoad = true;

    public PluginSystem(Kibibyte kibibyte) {
        this.kibibyte = kibibyte;
        pluginDirectory = new File("plugins");
        if (!pluginDirectory.exists()) {
            if (!pluginDirectory.mkdir())
                logger.log(LogLevel.WARNING, "Could not create plugin directory");
        }
        updatePluginList();
    }

    public void updatePluginList() {
        allPlugins = new HashMap<String, File>();
        File[] files = pluginDirectory.listFiles();
        if (files == null) {
            logger.log(LogLevel.WARNING, "listFiles() on plugindir was null, apperently the plugin directory is no directory.");
            return;
        }
        ArrayList<URL> urls = new ArrayList<URL>();
        for (File f : files) {
            if (f.isFile()) {
                try {
                    JarFile jf = new JarFile(f);
                    String name = jf.getManifest().getMainAttributes().getValue("kibi-plugin-name");
                    allPlugins.put(name, f);
                    if (name != null)
                        urls.add(f.toURI().toURL());
                } catch (Exception e) {

                }
            }
        }
        if (initialLoad)
            loader = new URLClassLoader(urls.toArray(new URL[0]));
        initialLoad = false;
    }

    public void loadPluginByName(String name) throws InvalidPluginException, InstantiationException {
        if (isLoaded(name))
            throw new RuntimeException("Plugin already loaded. to prevent this exception use isLoaded(name) to check if the plugin is already loaded");
        if (!pluginExists(name))
            throw new RuntimeException("Plugin does not exist.");
        try {
            File pfile = allPlugins.get(name);
            JarFile jf = new JarFile(pfile);
            String clazz = jf.getManifest().getMainAttributes().getValue("kibi-plugin-class");
            Class<?extends IPlugin> pclass = (Class<? extends IPlugin>) Class.forName(clazz, true, loader);
            IPlugin iPlugin = pclass.newInstance();
            Plugin plugin = new Plugin(kibibyte, name, iPlugin, loader);

            pluginlist.put(name, plugin);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            throw new InvalidPluginException("Declared Main Class does not exist");
        } catch (ClassCastException ex) {
            throw new InvalidPluginException("Mainclass doesn't implement IPlugin");
        } catch (IllegalAccessException e) {
            throw new InvalidPluginException("Default Constructor is not Public");
        }
    }

    public boolean isLoaded(String name) {
        return pluginlist.containsKey(name);
    }

    public boolean pluginExists(String name) {
        return allPlugins.containsKey(name);
    }

    public void loadPluginByClass(String clazz, String name) throws ClassNotFoundException, InvalidPluginException, InstantiationException {
        if (isLoaded(name))
            throw new RuntimeException("Plugin already loaded. to prevent this exception use isLoaded(name) to check if the plugin is already loaded");
        try {
            Class<?extends IPlugin> pclass = (Class<? extends IPlugin>) Class.forName(clazz);
            IPlugin iPlugin = pclass.newInstance();
            Plugin plugin = new Plugin(kibibyte, name, iPlugin, null);
            pluginlist.put(name, plugin);
        } catch (ClassCastException ex) {
            throw new InvalidPluginException("Mainclass doesn't implement IPlugin");
        } catch (IllegalAccessException e) {
            throw new InvalidPluginException("Default Constructor is not Public");
        }
    }

    public void enable(String name) {
        if (!isLoaded(name)) {
            throw new RuntimeException("Plugin not loaded");
        }
        pluginlist.get(name).enable();
    }

    public void disable(String name) {
        if (!isLoaded(name)) {
            throw new RuntimeException("Plugin not loaded");
        }
        pluginlist.get(name).disable();
    }

    public Set<String> listPlugins() {
        return allPlugins.keySet();
    }

    public boolean isEnabled(String name) {
        if (!isLoaded(name)) {
            throw new RuntimeException("Plugin not loaded");
        }
        return pluginlist.get(name).isEnabled();
    }
}
