package de.kilobyte22.app.kibibyte.plugin;

import de.kilobyte22.app.kibibyte.api.plugin.Bot;
import de.kilobyte22.app.kibibyte.core.Kibibyte;
import de.kilobyte22.app.kibibyte.exceptions.RehashException;
import org.pircbotx.exception.IrcException;
import org.pircbotx.exception.NickAlreadyInUseException;

import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: Stephan
 * Date: 10.03.13
 * Time: 20:12
 * To change this template use File | Settings | File Templates.
 */
public class BotImpl extends Bot {
    public BotImpl(BotAccess access, Kibibyte bot) {
        super(access, bot);
    }

    @Override
    public void join(String channel, String password) {
        bot.joinChannel(channel, password);
    }

    @Override
    public void part(String channel, String message) {
        bot.partChannel(bot.getChannel(channel), message);
    }

    @Override
    public void quit(String message) {
        bot.quitServer(message);
    }

    @Override
    public void sendRawLine(String line) {
        bot.sendRawLine(line);
    }

    @Override
    public void sendRawLineNow(String line) {
        bot.sendRawLineNow(line);
    }

    @Override
    public void sendMessage(String target, String message) {
        bot.sendMessage(target, message);
    }

    @Override
    public void sendNotice(String target, String message) {
        bot.sendNotice(target, message);
    }

    @Override
    public void sendCTCPCommand(String target, String command) {
        bot.sendCTCPCommand(target, command);
    }

    @Override
    public void sendCTCPResponse(String target, String command) {
        bot.sendCTCPResponse(target, command);
    }

    @Override
    public void changeNick(String newNick) {
        bot.changeNick(newNick);
    }

    @Override
    public void reconnect() throws IrcException, IOException {
        bot.reconnect();
    }

    @Override
    public void rehash() throws RehashException {
        bot.rehash();
    }
}
