package de.kilobyte22.app.kibibyte.plugin;

import de.kilobyte22.app.kibibyte.api.plugin.Bot;
import de.kilobyte22.app.kibibyte.command.CommandManager;
import de.kilobyte22.app.kibibyte.core.Kibibyte;
import de.kilobyte22.app.kibibyte.misc.Nickserv;
import de.kilobyte22.app.kibibyte.misc.PermissionSystem;
import de.kilobyte22.lib.configuration.ConfigFile;
import de.kilobyte22.lib.event.EventBus;
import de.kilobyte22.lib.logging.Logger;

import java.io.File;

/**
 * Created with IntelliJ IDEA.
 * User: Stephan
 * Date: 07.03.13
 * Time: 18:36
 *
 * @author Stephan
 * @copyright Copyright 2013 Stephan
 */
public class BotAccess {
    public ConfigFile config;
    public EventBus eventBus;
    public PermissionSystem permissionSystem;
    public Nickserv nickservSystem;
    public CommandManager commandManager;
    public Logger logger;
    public Bot bot;
    /**
     * Do not use unless there is no other way! this will go away later
     */
    @Deprecated public Kibibyte kibibyte; // Do not use unless necessary, it will eventually go away

    private EventBus.LockController eventBusLock;
    private Plugin plugin;
    private Kibibyte bot_;

    public BotAccess(Kibibyte bot, Plugin plugin) {
        this.plugin = plugin;
        this.bot_ = bot;
        this.bot = new BotImpl(this, bot);
        this.kibibyte = bot;

        eventBus = new EventBus(bot.eventBus);
        plugin.setEventBus(eventBus);
        eventBusLock = eventBus.new LockController();
        eventBusLock.setLockState(true); // BAM
        eventBusLock.setFrozen(true);

        permissionSystem = bot.permissionSystem;
        nickservSystem = bot.nickservSystem;
        commandManager = new CommandManager(bot.commandManager, this);
        commandManager.setEnabled(false);

        config = new ConfigFile(new File("config/" + plugin.getName() + ".yml"));
        logger = new Logger("PLUGIN/" + plugin.getName());
    }


    public String getPluginName() {
        return plugin.getName();
    }
}
