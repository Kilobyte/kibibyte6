package de.kilobyte22.app.kibibyte.plugin;

import de.kilobyte22.app.kibibyte.api.plugin.IPlugin;
import de.kilobyte22.app.kibibyte.core.Kibibyte;
import de.kilobyte22.lib.event.EventBus;

public class Plugin {
    private EventBus eventBus;
    private Kibibyte kibibyte;
    private EventBus.LockController ebLockController;
    private final ClassLoader loader;
    private IPlugin iPlugin;
    private String name;
    private BotAccess access;
    private boolean enabled = false;

    public Plugin(Kibibyte kibibyte, String name, IPlugin iPlugin, ClassLoader loader) {
        this.kibibyte = kibibyte;
        this.loader = loader;
        this.iPlugin = iPlugin;
        this.name = name;

        this.access = new BotAccess(kibibyte, this);
        iPlugin.load(access);
    }

    public void setEventBus(EventBus eventBus) {
        this.eventBus = eventBus;
        ebLockController = eventBus.new LockController();
    }

    public EventBus getEventBus() {
        return eventBus;
    }

    public String getName() {
        return name;
    }

    public IPlugin getIPlugin() {
        return iPlugin;
    }

    public void enable() {
        if (enabled)
            return;
        enabled = true;
        ebLockController.setFrozen(false);
        access.commandManager.setEnabled(true);
        iPlugin.enable();
    }

    public void disable() {
        if (!enabled)
            return;
        enabled = false;
        ebLockController.setFrozen(true);
        access.commandManager.setEnabled(false);
        iPlugin.disable();
    }

    public boolean isEnabled() {
        return enabled;
    }
}
